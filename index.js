// exercise 1
const numberEl = document.getElementById("number");

const check = () => {
  let total = 0;
  for (let i = 0; i < 10000; i++) {
    total += i;
    if (total > 10000) {
      numberEl.innerHTML = i;
      break;
    }
  }
};

// exercise 2

const xEl = document.getElementById("x");
const nEl = document.getElementById("n");
const sumEl = document.getElementById("sum");

const calc = () => {
  const xVal = +xEl.value;
  const nVal = +nEl.value;

  const sum = (x, n) => {
    let sum = 0;
    // sum = (x * (1 - x ** n)) / (1 - n);

    for (let i = 1; i <= n; i++) {
      sum += x ** i;
    }
    sumEl.innerHTML = sum;
  };

  sum(xVal, nVal);
};

// exercise 3
const numEl = document.getElementById("num");
const totalEl = document.getElementById("total");

const calculate = () => {
  const numVal = +numEl.value;

  const calcFactorial = (n) => {
    let total = 1;
    for (let i = 1; i <= n; i++) {
      total *= i;
    }
    totalEl.innerHTML = total;
  };
  calcFactorial(numVal);
};

// exercise 4
const resultEl = document.getElementById("result");

const oddEven = () => {
  let contentHTML = "";
  for (let i = 1; i <= 10; i++) {
    if (i % 2 === 0) {
      contentHTML += `<div style="background:red">Even ${i}</div>`;
    } else {
      contentHTML += `<div style="background:blue">Odd ${i}</div>`;
    }
  }
  resultEl.innerHTML = contentHTML;
};
